﻿using UnityEngine;

public class PlatformAScript : MonoBehaviour
{

    public GameObject playerPrefab;
   // public GameObject platformB;
    public GameObject[] platformB;
    void Start()
    {
        platformB[0].SetActive(false);
        platformB[1].SetActive(false);
    }


    private void OnTriggerEnter(Collider Platform)
    {
        if (Platform.gameObject.CompareTag("Player") == true)
        {

            Instantiate(playerPrefab);
            this.gameObject.SetActive(false);
            //Instantiate(platformB, new Vector3 (3f, -3.5f, -4f), Quaternion.identity);
            //Instantiate(platformB, new Vector3(3f, -3.5f, -8f), Quaternion.identity);
            platformB[0].SetActive(true);
            platformB[1].SetActive(true);


        }
    }

}
