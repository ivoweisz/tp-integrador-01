﻿using UnityEngine;

public class PlayerMovmentScript : MonoBehaviour
{
    public int speed;
    private Rigidbody rb;
    public int jumpForce;
    public CapsuleCollider col;

    private bool tocaPiso;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        tocaPiso = false;
    }


    private void FixedUpdate()
    {
        Vector3 m_Input = new Vector3(0, 0, Input.GetAxis("Horizontal"));

        rb.MovePosition(transform.position + m_Input * Time.deltaTime * speed);
    }

    private void Update()
    {
       
        if (Input.GetKeyDown("w") && tocaPiso)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            tocaPiso = false;
        }

    }

    // Se fija si esta en el piso
    private void OnCollisionEnter(Collision Player)
    {
        if (Player.gameObject.CompareTag("Piso"))
        {
            tocaPiso = true;
        }

        if (Player.gameObject.CompareTag("PlatformB") && this.gameObject.CompareTag("PlatformB1"))
        {
            Debug.Log("ganaste");

        }

    }

    //private void OnTriggerEnter(Collider Player)
    //{
       
    //}
}
